package com.hepta.mercado.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.hepta.mercado.entity.*;

public class ProdutoDAO {

	public void saveFab(Fabricante fabricante) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();

		try {
			System.out.println(fabricante.getNome());

			em.getTransaction().begin();
			System.out.println("teste2");
			em.persist(fabricante);
			System.out.println("teste3");
			em.getTransaction().commit();
			System.out.println("teste4");
		} catch (Exception e) {
			em.getTransaction().rollback();
			System.out.println(e.toString());
			e.printStackTrace();
			throw new Exception(e);
			
		} finally {
			em.close();
		}
	}

	
	
	public void save(Produto produto) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();

		try {
			System.out.println(produto.getNome());
			System.out.println(produto.getEstoque());
			System.out.println(produto.getUnidade());
			System.out.println(produto.getVolume());
			System.out.println(produto.getId());
			System.out.println(produto.getFabricante().getId());
			em.getTransaction().begin();
			System.out.println("teste2");
			em.persist(produto);
			System.out.println("teste3");
			em.getTransaction().commit();
			System.out.println("teste4");
		} catch (Exception e) {
			em.getTransaction().rollback();
			System.out.println(e.toString());
			e.printStackTrace();
			throw new Exception(e);
			
		} finally {
			em.close();
		}
	}

	public Produto update(Produto produto) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Produto produtoAtualizado = null;
		try {
			em.getTransaction().begin();
			produtoAtualizado = em.merge(produto);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return produtoAtualizado;
	}

	public void delete(Integer id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			em.getTransaction().begin();
			Produto produto = em.find(Produto.class, id);
			em.remove(produto);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}

	}

	public Produto find(Integer id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Produto produto = null;
		try {
			produto = em.find(Produto.class, id);
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return produto;
	}

	@SuppressWarnings("unchecked")
	public List<Produto> getAll() throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		List<Produto> produtos = new ArrayList<>();
		try {
			Query query = em.createQuery("FROM Produto");
			produtos = query.getResultList();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return produtos;
	}
	
	@SuppressWarnings("unchecked")
	public List<Fabricante> getAllFab() throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		List<Fabricante> fabricantes = new ArrayList<>();
		try {
			Query query = em.createQuery("FROM Fabricante");
			fabricantes = query.getResultList();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return fabricantes;
	}
}
