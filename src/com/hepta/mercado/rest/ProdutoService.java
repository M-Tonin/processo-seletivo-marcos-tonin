package com.hepta.mercado.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.FormParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.mercado.entity.Fabricante;
import com.hepta.mercado.entity.Produto;
import com.hepta.mercado.persistence.ProdutoDAO;

@Path("/produtos")
public class ProdutoService {
	
	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	private ProdutoDAO dao;
	
	public ProdutoService() {
		dao = new ProdutoDAO();
	}
	
	protected void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	/**
	 * Adiciona novo produto no mercado
	 * 
	 * @param produto: Novo produto
	 * @return response 200 (OK) - Conseguiu adicionar
	 */
	@Path("/saveContact")
//	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_HTML)
	@POST
	public Response produtoCreate(
			@FormParam("id") int id,
			@FormParam("produto") String produto_nome,
			@FormParam("estoque") int estoque,
			@FormParam("unidade") String unidade,
			@FormParam("volume") double volume,
			@FormParam("fabricante") int id_f) {
		
		Fabricante x = new Fabricante(); 
		x.setId(id_f);
		Produto produto = new Produto();
		produto.setEstoque(estoque);
		produto.setFabricante(x);
		//produto.setId(id);
		produto.setNome(produto_nome);
		produto.setUnidade(unidade);
		produto.setVolume(volume);
		try {
			dao.save(produto);
		} catch(Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
		}
		
		return Response.status(Status.OK).entity("<h4>Produto Salvo</h4>"
				+ "<a class=\"btn btn-dark\" href=\"/mercado/index.html\" role=\"button\">Home</a>").build();
	}
	

	@Path("/saveFabricante")
//	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_HTML)
	@POST
	public Response FabricanteCreate(
			@FormParam("fabricante") String fabricante) {
		
		Fabricante x = new Fabricante(); 
		x.setNome(fabricante);

		try {
			dao.saveFab(x);
		} catch(Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
		}
		
		return Response.status(Status.OK).entity("<h4>Fabricante Salvo</h4>"
				+ "<a class=\"btn btn-dark\" href=\"/mercado/index.html\" role=\"button\">Home</a>").build();
	}
	
	/**
	 * Lista todos os produtos do mercado
	 * 
	 * @return response 200 (OK) - Conseguiu listar
	 */
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response produtoRead() {
		List<Produto> produtos = new ArrayList<>();
		try {
			produtos = dao.getAll();
		} catch(Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar produtos").build();
		}
		
		GenericEntity<List<Produto>> entity = new GenericEntity<List<Produto>>(produtos) {};
		return Response.status(Status.OK).entity(entity).build();
	}
	
	/**
	 * Lista todos os produtos do mercado
	 * 
	 * @return response 200 (OK) - Conseguiu listar
	 */
	@Path("/fabricante")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response FabricanteRead() {
		List<Fabricante> fabricantes = new ArrayList<>();
		try {
			fabricantes = dao.getAllFab();
		} catch(Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar fabricantes").build();
		}
		
		GenericEntity<List<Fabricante>> entity = new GenericEntity<List<Fabricante>>(fabricantes) {};
		return Response.status(Status.OK).entity(entity).build();
	}
	
	
	/**
	 * Atualiza um produto no mercado
	 * 
	 * @param id: id do produto
	 * @param produto: Produto atualizado
	 * @return response 200 (OK) - Conseguiu atualiza
	 */
	@Path("/update/{id}")
	//@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_HTML)
	@GET
	public Response produtoUpdate(@PathParam("id") Integer id, Produto produto) {
		
		System.out.println(id);
			try {
				dao.update(produto);
			} catch(Exception e) {
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao atualizar produto").build();
			}
			
			return Response.status(Status.OK).entity("<h4>Produto Atualizado<h4>"
					+ "<a class=\"btn btn-dark\" href=\"/mercado/index.html\" role=\"button\">Home</a>").build();
			
	}
	
	/**
	 * Remove um produto do mercado
	 * 
	 * @param id: id do produto
	 * @return response 200 (OK) - Conseguiu remover
	 */
	@Path("delete/{id}")
	//@Produces(MediaType.APPLICATION_JSON)
	//@DELETE
	@GET
	public Response produtoDelete(@PathParam("id") Integer id) {
		
		System.out.println(id);
		try {
			dao.delete(id);
		} catch(Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao excluir produto").build();
		}
		
		return Response.status(Status.OK).entity("<h4>Produto Excluido</h4>"
				+ "<a class=\"btn btn-dark\" href=\"/mercado/index.html\" role=\"button\">Home</a>").build();
		
	}


	@Path("/atualizar")
	//@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_HTML)
	@POST
	public Response produtoAtualizar( 
			@FormParam("id") int id,
			@FormParam("produto") String produto_nome,
			@FormParam("estoque") int estoque,
			@FormParam("unidade") String unidade,
			@FormParam("volume") double volume,
			@FormParam("fabricante") int id_f) {
		
		Fabricante x = new Fabricante(); 
		x.setId(id_f);
		Produto produto = new Produto();
		produto.setId(id);
		produto.setEstoque(estoque);
		produto.setFabricante(x);
		//produto.setId(id);
		produto.setNome(produto_nome);
		produto.setUnidade(unidade);
		produto.setVolume(volume);
		
		System.out.println(id);
			try {
				dao.update(produto);
			} catch(Exception e) {
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao atualizar produto").build();
			}
			
			return Response.status(Status.OK).entity("<h4>Produto Atualizado<h4>"
					+ "<a class=\"btn btn-dark\" href=\"/mercado/index.html\" role=\"button\">Home</a>").build();
			
	}
	
	
}


